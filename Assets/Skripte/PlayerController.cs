﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
	
	public float speed;
	
	public GUIText winTxt;
	public GUIText score;
	private int count;
	
	void Start(){
		count = 0;
		winTxt.text = "";
		postaviGUIRezultat(score, count);
	}
	
	void FixedUpdate() {
		float moveHorizontal = Input.GetAxis("Horizontal");
		float moveVetical = Input.GetAxis("Vertical");
		
		Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVetical);
		rigidbody.AddForce(movement * speed * Time.deltaTime);
	}
	
	void OnTriggerEnter(Collider other){
		
		if (other.gameObject.tag == "PickUp"){
			
			//Destroy(other.gameObject);
			other.gameObject.SetActive (false);
			
			count++;
			postaviGUIRezultat(score, count);
			if(GameObject.FindGameObjectsWithTag("PickUp").Length == 0){
				winTxt.text = "Pobjedio si!";
			}
		}
		
	}
	
	private void postaviGUIRezultat(GUIText score, int count){
		score.text = "Rezultat: " + count.ToString();
	}
}